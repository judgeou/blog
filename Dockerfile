FROM node:alpine

WORKDIR /usr/app

COPY ["package.json", "./"]

RUN yarn

COPY scaffolds scaffolds
COPY themes themes
COPY _config.yml .
COPY source source

RUN yarn run build

CMD ["yarn", "run", "start"]
